using System;
using HarmonyLib;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;

// namespace ponyraces
// {
//     // doesnt do shit >:^(

//     [HarmonyPatch(typeof(EntityBehaviorInterpolatePosition))]
//     [HarmonyPatch("OnRenderFrame")]
//     class PatchBehaviorInterpolatePosition
//     {
//         // modifying:
//         // https://github.com/anegostudios/vsessentialsmod/blob/f102718b2ae790e9290340d1bca008535588d7c4/Entity/Behavior/BehaviorInterpolatePosition.cs#L36
//         // to stop interpolation for entity mounted on another entity
//         static bool Prefix(EntityBehaviorInterpolatePosition __instance)
//         {
//             var entity = __instance.entity;

//             bool isMounted = (entity as EntityAgent)?.MountedOn != null;

//             Console.WriteLine($"[Interpolate OnRenderFrame] isMounted: {isMounted}, entity.Pos = {entity.Pos.XYZ}");

//             return false;
//         }
//     }

//     [HarmonyPatch(typeof(Vintagestory.GameContent.EntityBehaviorPlayerPhysics))]
//     [HarmonyPatch("HandleRemotePhysics")]
//     class PatchBehaviorPlayerPhysics1
//     {
//         static void Postfix(Vintagestory.GameContent.EntityBehaviorPlayerPhysics __instance)
//         {
//             var entity = __instance.entity;

//             bool isMounted = (entity as EntityAgent)?.MountedOn != null;

//             Console.WriteLine($"[HandleRemotePhysics] isMounted: {isMounted} pos: {entity.Pos}");
//         }
//     }

//     [HarmonyPatch(typeof(Vintagestory.GameContent.EntityBehaviorPlayerPhysics))]
//     [HarmonyPatch("SimPhysics")]
//     class PatchBehaviorPlayerPhysicsSimPhysics
//     {
//         static void Postfix(Vintagestory.GameContent.EntityBehaviorPlayerPhysics __instance)
//         {
//             var entity = __instance.entity;

//             bool isMounted = (entity as EntityAgent)?.MountedOn != null;

//             Console.WriteLine($"[SimPhysics] isMounted: {isMounted} pos: {entity.Pos}");
//         }
//     }

//     [HarmonyPatch(typeof(Vintagestory.GameContent.EntityPlayerShapeRenderer))]
//     [HarmonyPatch("GetOtherPlayerRenderOffset")]
//     class PatchEntityPlayerShapeRendererRenderOffset
//     {
//         static void Postfix(Vintagestory.GameContent.EntityPlayerShapeRenderer __instance)
//         {
//             var entity = __instance.entity;

//             bool isMounted = (entity as EntityAgent)?.MountedOn != null;

//             Console.WriteLine($"[GetOtherPlayerRenderOffset] isMounted: {isMounted} pos: {entity.Pos.XYZ}");
//         }
//     }
// }