using HarmonyLib;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using kemono;


namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.GameContent.CharacterSystem))]
    [HarmonyPatch("Event_MatchesGridRecipe")]
    class PatchCharacterAlchemyRecipes
    {
        // modifying:
        // https://github.com/anegostudios/vssurvivalmod/blob/ac138f05ab0faf6346177997c74a09c2ee3a9f6a/Systems/Character/Character.cs#L387
        static bool Prefix(bool __result, IPlayer player, ref GridRecipe recipe)
        {
            if (recipe.Name != null && PonyRaceMod.AlchemyRecipeNames.Contains(recipe.Name.Path))
            {
                var kemono = player.Entity.World.Api.ModLoader.GetModSystem<KemonoMod>();
                // check if player has required alchemy trait, otherwise cancel
                if (!kemono.HasRaceTrait(player.Entity, PonyRaceMod.AlchemyAllowedTrait))
                {
                    __result = false;
                    return false; // skip original
                }
            }

            return true; // run original
        }
    }
}
