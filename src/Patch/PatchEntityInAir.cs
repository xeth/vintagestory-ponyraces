using HarmonyLib;
using System;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;


namespace ponyraces
{
    [HarmonyPatch(typeof(PModulePlayerInAir))]
    [HarmonyPatch("ApplyFlying")]
    class PatchPModulePlayerInAirApplyFlying
    {
        // applies speed modifier while flying, so that flying ponies cant 
        // move retardedly fast in full plate armor
        // https://github.com/anegostudios/vsapi/blob/1bea61d7f13015653b2a6c04c64a4b769fb38a40/Common/Entity/Physics/Player/PModulePlayerInAir.cs#L41
        static bool Prefix(ref float dt, ref Entity entity, ref EntityPos pos, ref EntityControls controls)
        {
            if (controls.IsFlying && !controls.Gliding)
            {
                double deltaY = controls.FlyVector.Y;
                if (controls.Up || controls.Down)
                {
                    float moveSpeed = Math.Min(0.2f, dt) * GlobalConstants.BaseMoveSpeed * controls.MovespeedMultiplier / 2;
                    deltaY = (controls.Up ? moveSpeed : 0) + (controls.Down ? -moveSpeed : 0);
                }

                // Prevent entities from flying too close to dimension boundaries
                if (deltaY > 0 && pos.Y % BlockPos.DimensionBoundary > BlockPos.DimensionBoundary * 3 / 4)
                {
                    deltaY = 0;
                }

                // multiplier for flying speed
                float speedMultiplier = entity.Stats.GetBlended("walkspeed");

                pos.Motion.Add(speedMultiplier * controls.FlyVector.X, deltaY, speedMultiplier * controls.FlyVector.Z);

                return false; // skip original
            }

            return true; // run original
        }
    }
}
