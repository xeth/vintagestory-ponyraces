﻿using ProtoBuf;
using HarmonyLib;
using System;
using System.Diagnostics;
using System.IO;
using Tomlyn;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using kemono;
using System.Collections.Generic;


[assembly: ModInfo("ponyrace",
    Description = "pony races for horse mod",
    Website     = "",
    Authors     = new []{ "xeth" })]
namespace ponyraces
{

    /// <summary>
    /// Packet from server to client requesting to play an animation.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerStartAnimation
    {
        public long EntityId;
        public string Code;
    }

    /// <summary>
    /// Packet from server to client requesting to stop an animation.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerStopAnimation
    {
        public long EntityId;
        public string Code;
    }

    /// <summary>
    /// Packet from client to server requesting fly.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestFly
    {
        
    }

    /// <summary>
    /// Packet from client to server requesting horn light.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestLight
    {
        
    }

    /// <summary>
    /// Packet from client to server requesting night vision.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestNightVision
    {
        
    }

    /// <summary>
    /// Packet from client to server requesting buck.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestBuck
    {
        
    }

    /// <summary>
    /// Packet from client to server requesting use kirin fire.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestKirinFire
    {
        
    }

    /// <summary>
    /// Packet from server to client to spawn kirin fire particles.
    /// Contains x,y,z location of fire.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerKirinFireParticles
    {
        public float X;
        public float Y;
        public float Z;
    }

    /// <summary>
    /// Packet from client to server requesting use levitate ability.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientRequestLevitate
    {
        public long Target;
    }

    /// <summary>
    /// Packet from client to server indicating player has stopped levitating
    /// target entity (e.g. when hotkey is released).
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientStopLevitate
    {
        
    }

    /// <summary>
    /// Packet from server to client indicating levitate has started on a
    /// target entity.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerLevitateStart
    {
        // entity id using the levitate
        public long EntityId;
        // target id being levitated
        public long TargetId;
    }

    /// <summary>
    /// Packet from server to client indicating entity id has stopped
    /// levitating a target.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerLevitateStop
    {
        public long EntityId;
    }

    /// <summary>
    /// Pony race mod config.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PonyRaceConfig
    {
        // NOTE: must use all properties with get/set for Tomlyn to parse.

        /// <summary>
        /// Since some people seethed about high crop drop rate penalty
        /// for pegasus and unicorn for singleplayer, this multiplier adjusts
        /// the impact of crop drop rate modifiers. The final drop rate 
        /// multiplier is:
        ///     `finalDropRate = 1 + (playerDropRate - 1) * cropDropRateEffect`
        /// So setting this `cropDropRateEffect < 1` will reduce the effects
        /// of player crop drop rate stat, while setting `cropDropRateEffect > 1`
        /// will increase the effects.
        /// </summary>
        public float CropDropRateEffect { get; set; } = 1.0f;

        /// <summary>
        /// Hunger cost for bucking fruit trees.
        /// </summary>
        public float BuckFruitTreeHunger { get; set; } = 40.0f;
        
        /// <summary>
        /// Hunger cost for bucking berry bushes.
        /// </summary>
        public float BuckBerryBushHunger { get; set; } = 20.0f;
        
        /// <summary>
        /// Hunger cost for bucking regular trees (for sticks and saplings).
        /// </summary>
        public float BuckTreeHunger { get; set; } = 60.0f;

        /// <summary>
        /// Probability a leaf block will break and drop a stick
        /// </summary>
        public double BuckTreeStickDropRate { get; set; } = 0.20;

        /// <summary>
        /// Probability a leaf block will break and also drop a sapling
        /// </summary>
        public double BuckTreeSaplingDropRate { get; set; } = 0.06;

        /// <summary>
        /// Global flight stamina loss rate, adjust to fly longer/shorter.
        /// </summary>
        public float FlyStaminaLoss { get; set; } = 1.0f;

        /// <summary>
        /// Increased flight stamina loss multiplier while flying with a
        /// player mounted on back.
        /// </summary>
        public float FlyStaminaLossMountedMultiplier { get; set; } = 1.25f;

        /// <summary>
        /// Light threshold for "darkness", which affects stamina loss
        /// for ponies that can only fly in dark (bats).
        /// </summary>
        public float FlyDarkThreshold { get; set; } = 14.0f;

        /// <summary>
        /// Additional flight stamina loss multiplier per light level
        /// above darkness threshold, based on a ramp function:
        ///               /
        ///              /
        ///   1   ______/        --> light
        ///             x
        ///       "Dark" threshold ~14
        /// This parameter sets the slope above threshold.
        /// </summary>
        public float FlyOutsideDarkLossMultiplier { get; set; } = 0.5f;

        /// <summary>
        /// Hunger loss per second while flying.
        /// </summary>
        public float FlyHungerLossRate { get; set; } = 10f;

        /// <summary>
        /// Increased hunger loss multiplier while flying with a player
        /// mounted on back.
        /// </summary>
        public float FlyHungerLossMountedMultiplier { get; set; } = 1.5f;

        /// <summary>
        /// Kirin loses temporal stability if below this health threshold.
        /// </summary>
        public double NirikHealthStabilityLossThreshold { get; set; } = 0.5;

        /// <summary>
        /// Kirin temporal stability percent loss rate per second
        /// vs health amount under threshold. 
        /// Default: 5% temporal stability loss per second when hp ~ 0
        /// </summary>
        public double NirikHealthStabilityLossRate { get; set; } = 0.1;

        /// <summary>
        /// Nirik mode enabled under this temporal stability threshold.
        /// </summary>
        public double NirikTemporalStabilityThreshold { get; set; } = 0.5;

        /// <summary>
        /// Kirin fire temporal stability cost
        /// </summary>
        public float KirinFireStabilityCost { get; set; } = 0.05f;

        /// <summary>
        /// Kirin fire hunger cost
        /// </summary>
        public float KirinFireHungerCost { get; set; } = 50;

        /// <summary>
        /// Kirin fire temperature added to anvil item
        /// </summary>
        public float KirinFireAnvilTemperatureAdd { get; set; } = 500;

        /// <summary>
        /// Kirin fire max anvil item temperature allowed
        /// </summary>
        public float KirinFireAnvilTemperatureMax { get; set; } = 1200;

        /// <summary>
        /// Unicorn levitate ability hunger cost per second.
        /// </summary>
        public float LevitateHungerCost { get; set; } = 100;

        /// <summary>
        /// Unicorn levitate ability stamina cost per second. Note this
        /// must be balanced against intrinsic stamina regen rate.
        /// </summary>
        public float LevitateStaminaCost { get; set; } = 0.75f;

        /// <summary>
        /// Max entity velocity allowed when stopping levitate and "throwing"
        /// an entity in a direction in blocks/second. E.g. when you move
        /// your screen fast and stop levitating to throw an entity,
        /// this limits the max velocity the entity can be thrown at.
        /// </summary>
        public double LevitateMaxThrowVelocity { get; set; } = 8;

        /// <summary>
        /// List of alchemy recipe names, default integration is intended for
        /// "alchemy" mod with recipes from:
        /// https://github.com/Llama3013/vsmod-Alchemy/tree/main/resources/assets/alchemy/recipes/grid/ingredient
        /// </summary>
        public string[] AlchemyRecipes { get; set; } = new string[] {
            "recipes/grid/ingredient/archerbase.json",
            "recipes/grid/ingredient/basicbase.json",
            "recipes/grid/ingredient/glowbase.json",
            "recipes/grid/ingredient/healingeffectbase.json",
            "recipes/grid/ingredient/hungerenhancebase.json",
            "recipes/grid/ingredient/hungersupressbase.json",
            "recipes/grid/ingredient/hunterbase.json",
            "recipes/grid/ingredient/looterbase.json",
            "recipes/grid/ingredient/meleebase.json",
            "recipes/grid/ingredient/miningbase.json",
            "recipes/grid/ingredient/nutritionbase.json",
            "recipes/grid/ingredient/poisonbase.json",
            "recipes/grid/ingredient/predatorbase.json",
            "recipes/grid/ingredient/recallbase.json",
            "recipes/grid/ingredient/regenbase.json",
            "recipes/grid/ingredient/scentmaskbase.json",
            "recipes/grid/ingredient/speedbase.json",
            "recipes/grid/ingredient/vitalitybase.json",
            "recipes/grid/ingredient/waterbreathebase.json",
            "recipes/grid/ingredient/wildarcherbase.json",
            "recipes/grid/ingredient/wildhealingeffectbase.json",
            "recipes/grid/ingredient/wildhungerenhancebase.json",
            "recipes/grid/ingredient/wildhungersupressbase.json",
            "recipes/grid/ingredient/wildhunterbase.json",
            "recipes/grid/ingredient/wildlooterbase.json",
            "recipes/grid/ingredient/wildmeleebase.json",
            "recipes/grid/ingredient/wildminingbase.json",
            "recipes/grid/ingredient/wildregenbase.json",
            "recipes/grid/ingredient/wildscentmaskbase.json",
            "recipes/grid/ingredient/wildspeedbase.json",
            "recipes/grid/ingredient/wildvitalitybase.json",
        };
    }

    /// <summary>
    /// Packet from server to client to sync mod config.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerModConfig
    {
        public PonyRaceConfig Config;
    }

    // IRenderer to implement nightvision, see reference:
    // https://github.com/anegostudios/vssurvivalmod/blob/master/Systems/JonasDevices/ItemNightVisionDevice.cs
    public partial class PonyRaceMod : ModSystem, IRenderer
    {
        public static string PATH { get; } = "ponyraces";

        // constants
        public static string[] NightVisionAllowedTraits = {"nightaffinity"};
        public static string[] BuckAllowedTraits = {"buck"};
        public static string[] KirinFireAllowedTraits = {"nirik"};
        public static string[] LevitateAllowedTraits = {"levitate"};
        public static string AlchemyAllowedTrait = "alchemist";
        // set of all "alchemy" recipes, global that will be set by
        // config loading, evil global - cope about it
        public static HashSet<string> AlchemyRecipeNames = new HashSet<string>();

        public double RenderOrder => 0.001; // has to be greater than night vision device = 0
        public int RenderRange => 1;

        // mod config (should be synced from server to client)
        public PonyRaceConfig Config { get; private set; } = new PonyRaceConfig();

        // server and client
        internal ICoreAPI api;
        internal KemonoMod kemono; // kemono instance

        // server
        internal ICoreServerAPI sapi;
        internal IServerNetworkChannel serverToClientChannel;

        // client
        internal ICoreClientAPI capi;
        internal IClientNetworkChannel clientToServerChannel;
        bool NightVisionEnabled = false;
        float NightVisionStrength = 0.6f;

        public override void Start(ICoreAPI api)
        {
            this.api = api;

            kemono = api.ModLoader.GetModSystem<KemonoMod>();

            api.Event.RegisterGameTickListener(OnSlowTick, 1000);

            api.RegisterEntityBehaviorClass("nirik", typeof(EntityBehaviorNirik));
            api.RegisterEntityBehaviorClass("ponylight", typeof(EntityBehaviorPonyLight));
            api.RegisterEntityBehaviorClass("ponyflight", typeof(EntityBehaviorPonyFlight));
            api.RegisterEntityBehaviorClass("ponystamina", typeof(EntityBehaviorPonyStamina));
            api.RegisterEntityBehaviorClass("ponylevitate", typeof(EntityBehaviorPonyLevitate));

            api.RegisterMountable("ponymount", EntityPonyMount.GetMountable);
            api.RegisterEntity("EntityPonyMount", typeof(EntityPonyMount));

            var harmony = new Harmony("ponyraces");
            harmony.PatchAll();

            // set initial alchemy recipe names
            AlchemyRecipeNames = new HashSet<string>(Config.AlchemyRecipes);
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            capi = api;

            api.Event.RegisterRenderer(this, EnumRenderStage.Before, "ponynightvision");

            api.Event.RegisterGameTickListener(OnClientStopMountable, 100);

            clientToServerChannel =
                api.Network.RegisterChannel("ponyrace")
                .RegisterMessageType<PacketClientRequestFly>()
                .RegisterMessageType<PacketClientRequestLight>()
                .RegisterMessageType<PacketClientRequestNightVision>()
                .RegisterMessageType<PacketClientRequestBuck>()
                .RegisterMessageType<PacketClientRequestKirinFire>()
                .RegisterMessageType<PacketClientRequestLevitate>()
                .RegisterMessageType<PacketClientStopLevitate>()
                .RegisterMessageType<PacketClientMount>()
                .RegisterMessageType<PacketClientUnmount>()
                .RegisterMessageType<PacketClientMountableStart>()
                .RegisterMessageType<PacketClientMountableStop>()
                .RegisterMessageType<PacketServerModConfig>()
                .RegisterMessageType<PacketServerStartAnimation>()
                .RegisterMessageType<PacketServerStopAnimation>()
                .RegisterMessageType<PacketServerKirinFireParticles>()
                .RegisterMessageType<PacketServerLevitateStart>()
                .RegisterMessageType<PacketServerLevitateStop>()
                .SetMessageHandler<PacketServerModConfig>(OnClientReceiveModConfig)
                .SetMessageHandler<PacketServerStartAnimation>(OnClientReceiveStartAnimation)
                .SetMessageHandler<PacketServerStopAnimation>(OnClientReceiveStopAnimation)
                .SetMessageHandler<PacketServerKirinFireParticles>(OnClientReceiveKirinFireParticles)
                .SetMessageHandler<PacketServerLevitateStart>(OnClientReceiveLevitateStart)
                .SetMessageHandler<PacketServerLevitateStop>(OnClientReceiveStopLevitate)
            ;

            // fly hotkey
            capi.Input.RegisterHotKey("ponyflight", Lang.Get("ponyraces:hotkey-pony-flight"), GlKeys.Space, HotkeyType.MovementControls);
            capi.Input.SetHotKeyHandler("ponyflight", OnHotkeyPonyFlight);
            capi.Input.HotKeys["ponyflight"].DefaultMapping.SecondKeyCode = (int) GlKeys.Space;
            var currentFlyKey = capi.Input.HotKeys["ponyflight"].CurrentMapping.KeyCode;
            if (currentFlyKey == (int) GlKeys.Space)
            {
                capi.Input.HotKeys["ponyflight"].CurrentMapping.SecondKeyCode = (int) GlKeys.Space;
            }

            // horn light hotkey
            capi.Input.RegisterHotKey("ponylight", Lang.Get("ponyraces:hotkey-pony-light"), GlKeys.L, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponylight", OnHotkeyPonyLight);

            // night vision hotkey
            capi.Input.RegisterHotKey("ponynightvision", Lang.Get("ponyraces:hotkey-pony-nightvision"), GlKeys.N, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponynightvision", OnHotkeyPonyNightVision);

            // buck hotkey
            capi.Input.RegisterHotKey("ponybuck", Lang.Get("ponyraces:hotkey-pony-buck"), GlKeys.K, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponybuck", OnHotkeyBuck);

            // kirin fire hotkey
            capi.Input.RegisterHotKey("ponykirinfire", Lang.Get("ponyraces:hotkey-pony-kirinfire"), GlKeys.I, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponykirinfire", OnHotkeyKirinFire);

            // unicorn levitation hotkey
            capi.Input.RegisterHotKey("ponylevitate", Lang.Get("ponyraces:hotkey-pony-levitate"), GlKeys.V, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponylevitate", OnHotkeyLevitate);

            // allow mount hotkey
            capi.Input.RegisterHotKey("ponymountable", Lang.Get("ponyraces:hotkey-pony-mountable"), GlKeys.U, HotkeyType.CharacterControls);
            capi.Input.SetHotKeyHandler("ponymountable", OnHotkeyMountable);

            capi.Gui.RegisterDialog(new HudElementFlightBar(capi));
            capi.Gui.RegisterDialog(new HudElementStamina(capi));
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            sapi = api;

            serverToClientChannel =
                api.Network.RegisterChannel("ponyrace")
                .RegisterMessageType<PacketClientRequestFly>()
                .RegisterMessageType<PacketClientRequestLight>()
                .RegisterMessageType<PacketClientRequestNightVision>()
                .RegisterMessageType<PacketClientRequestBuck>()
                .RegisterMessageType<PacketClientRequestKirinFire>()
                .RegisterMessageType<PacketClientRequestLevitate>()
                .RegisterMessageType<PacketClientStopLevitate>()
                .RegisterMessageType<PacketClientMount>()
                .RegisterMessageType<PacketClientUnmount>()
                .RegisterMessageType<PacketClientMountableStart>()
                .RegisterMessageType<PacketClientMountableStop>()
                .RegisterMessageType<PacketServerModConfig>()
                .RegisterMessageType<PacketServerStartAnimation>()
                .RegisterMessageType<PacketServerStopAnimation>()
                .RegisterMessageType<PacketServerKirinFireParticles>()
                .RegisterMessageType<PacketServerLevitateStart>()
                .RegisterMessageType<PacketServerLevitateStop>()
                .SetMessageHandler<PacketClientRequestFly>(OnServerReceiveRequestFly)
                .SetMessageHandler<PacketClientRequestLight>(OnServerReceiveRequestLight)
                .SetMessageHandler<PacketClientRequestNightVision>(OnServerReceiveRequestNightVision)
                .SetMessageHandler<PacketClientRequestBuck>(OnServerReceiveRequestBuck)
                .SetMessageHandler<PacketClientRequestKirinFire>(OnServerReceiveRequestKirinFire)
                .SetMessageHandler<PacketClientRequestLevitate>(OnServerReceiveRequestLevitate)
                .SetMessageHandler<PacketClientRequestLevitate>(OnServerReceiveRequestLevitate)
                .SetMessageHandler<PacketClientStopLevitate>(OnServerReceiveStopLevitate)
                .SetMessageHandler<PacketClientMount>(OnServerReceiveMount)
                .SetMessageHandler<PacketClientUnmount>(OnServerReceiveUnmount)
                .SetMessageHandler<PacketClientMountableStart>(OnServerReceiveMountableStart)
                .SetMessageHandler<PacketClientMountableStop>(OnServerReceiveMountableStop)
            ;

            // register commands
            var parsers = api.ChatCommands.Parsers;

            api.ChatCommands
                .Create("ponyraces")
                .RequiresPrivilege(Privilege.controlserver)
                .WithArgs(parsers.OptionalWord("subcmd"))
                .HandleWith(OnCmdPonyRaces)
            ;

            // load mod config
            ReloadModConfig();
            
            api.Event.PlayerJoin += Event_PlayerJoinServer;
        }

        /// <summary>
        /// Returns true or false if mod config was successfully reloaded.
        /// </summary>
        /// <returns></returns>
        public bool ReloadModConfig()
        {
            var pathConfig = Path.Combine(GamePaths.ModConfig, "ponyraces.toml");

            // if mod config file does not exist, write default resource to it
            if (!Path.Exists(pathConfig))
            {
                var resource = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("ponyraces.resources.modconfig.ponyraces.toml");
                if (resource == null)
                {
                    // failed to load default config wtf
                    api.Logger.Error($"[ponyraces] Failed to load mod config resource ponyraces.toml");
                    return false;
                }

                // write to default config path
                using (var file = new FileStream(pathConfig, FileMode.Create, FileAccess.Write))
                {
                    api.Logger.Notification($"[ponyraces] Saving default config to {pathConfig}");
                    resource.CopyTo(file);
                }
            }

            try
            {
                // this raises exception if invalid toml syntax or if invalid
                // fields present, so better to wrap in try/catch
                string configToml = File.ReadAllText(pathConfig);
                Config = Toml.ToModel<PonyRaceConfig>(configToml);

                // set alchemy recipe names
                AlchemyRecipeNames = new HashSet<string>(Config.AlchemyRecipes);

                // send to all online players
                serverToClientChannel.BroadcastPacket(new PacketServerModConfig { Config = Config });
            }
            catch (Exception e)
            {
                api.Logger.Error($"Failed to load mod config from {pathConfig}: {e}");
                return false;
            }

            return true;
        }

        public void Event_PlayerJoinServer(IServerPlayer byPlayer)
        {
            // sync mod config to client
            serverToClientChannel.SendPacket(new PacketServerModConfig { Config = Config }, byPlayer);
        }

        public void OnClientReceiveModConfig(PacketServerModConfig p)
        {
            Config = p.Config;

            // set alchemy recipe names
            AlchemyRecipeNames = new HashSet<string>(p.Config.AlchemyRecipes);
        }

        public void OnSlowTick(float dt)
        {
            foreach (var player in api.World.AllOnlinePlayers)
            {
                if (player.Entity == null) continue;

                // block range modifier
                var gameMode = player.WorldData.CurrentGameMode;
                if (gameMode == EnumGameMode.Guest || gameMode == EnumGameMode.Survival)
                {
                    float blockRangeModifier = player.Entity.Stats.GetBlended("blockrange");
                    player.WorldData.PickingRange = GlobalConstants.DefaultPickingRange * blockRangeModifier;
                }

                if (api.Side == EnumAppSide.Client && player == capi.World.Player)
                {
                    // check and disable nightvision if dont have traits
                    if (NightVisionEnabled)
                    {
                        bool canNightVision = kemono.HasAnyRaceTrait(player.Entity, NightVisionAllowedTraits);
                        if (!canNightVision)
                        {
                            NightVisionEnabled = false;
                            clientToServerChannel.SendPacket(new PacketClientRequestNightVision());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Custom packet handler for starting client animations from server.
        /// Server side `entity.AnimManager.StartAnimation` does not work
        /// (wtf?).
        /// </summary>
        /// <param name="p"></param>
        public void OnClientReceiveStartAnimation(PacketServerStartAnimation p)
        {
            var entity = capi.World.GetEntityById(p.EntityId);
            if (entity == null) return;

            entity.AnimManager.StartAnimation(new AnimationMetaData() {
                Animation = p.Code,
                Code = p.Code,
                ClientSide = true,
            }.Init());
        }

        /// <summary>
        /// Custom packet handler for stopping client animations from server.
        /// </summary>
        /// <param name="p"></param>
        public void OnClientReceiveStopAnimation(PacketServerStopAnimation p)
        {
            var entity = capi.World.GetEntityById(p.EntityId);
            if (entity == null) return;
            entity.AnimManager.StopAnimation(p.Code);
        }

        public bool OnHotkeyPonyFlight(KeyCombination keys)
        {
            var player = capi.World.Player;
            var gameMode = player.WorldData.CurrentGameMode;
            if (gameMode == EnumGameMode.Creative || gameMode == EnumGameMode.Spectator)
            {
                return true;
            }

            // client-side check if can fly
            bool canFly = kemono.HasAnyRaceTrait(player.Entity, EntityBehaviorPonyFlight.FlightTraits);
            if (!canFly) return true;

            // send packet to server requesting to fly
            clientToServerChannel.SendPacket(new PacketClientRequestFly());

            return true;
        }

        public void OnServerReceiveRequestFly(IServerPlayer fromPlayer, PacketClientRequestFly p)
        {
            var player = fromPlayer.Entity;
            var gameMode = fromPlayer.WorldData.CurrentGameMode;
            if (gameMode == EnumGameMode.Creative || gameMode == EnumGameMode.Spectator)
            {
                return;
            }

            // server-side check if can fly
            bool canFly = kemono.HasAnyRaceTrait(player, EntityBehaviorPonyFlight.FlightTraits);
            if (!canFly) return;

            // toggle fly
            var flyBehavior = player.GetBehavior<EntityBehaviorPonyFlight>();
            flyBehavior?.ToggleFly();
        }

        public bool OnHotkeyPonyLight(KeyCombination keys)
        {
            var player = capi.World.Player;

            // client-side check if can use
            bool canLight = kemono.HasAnyRaceTrait(player.Entity, EntityBehaviorPonyLight.AllowedTraits);
            if (!canLight) return true;

            // send packet to server requesting to fly
            clientToServerChannel.SendPacket(new PacketClientRequestLight());

            return true;
        }

        public void OnServerReceiveRequestLight(IServerPlayer fromPlayer, PacketClientRequestLight p)
        {
            var player = fromPlayer.Entity;

            // server-side check if can light
            bool canLight = kemono.HasAnyRaceTrait(player, EntityBehaviorPonyLight.AllowedTraits);
            if (!canLight) return;

            // broadcast light behavior toggle
            var light = fromPlayer.Entity.GetBehavior<EntityBehaviorPonyLight>();
            if (light != null)
            {
                light.Toggle();
            }
        }

        public bool OnHotkeyPonyNightVision(KeyCombination keys)
        {
            var player = capi.World.Player;

            // check if can use
            bool canNightVision = kemono.HasAnyRaceTrait(player.Entity, NightVisionAllowedTraits);
            if (!canNightVision) return true;

            // toggle night vision
            NightVisionEnabled = !player.Entity.WatchedAttributes.GetBool("ponynightvision", false);
            
            // send packet to server requesting night vision (makes eyes glow)
            clientToServerChannel.SendPacket(new PacketClientRequestNightVision());

            return true;
        }


        public void OnServerReceiveRequestNightVision(IServerPlayer fromPlayer, PacketClientRequestNightVision p)
        {
            var entity = fromPlayer.Entity;

            // server-side check if can light
            bool canLight = kemono.HasAnyRaceTrait(entity, NightVisionAllowedTraits);
            if (!canLight) return;

            // toggle enabled state
            bool enabled = !entity.WatchedAttributes.GetBool("ponynightvision", false);
            entity.WatchedAttributes.SetBool("ponynightvision", enabled);

            // night vision: make eyes glow to all players
            var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            if (skin != null)
            {
                skin.SetSkinPartGlow("eye", enabled ? 255 : 0, false);
                entity.WatchedAttributes.MarkPathDirty("skinConfig");
            }
        }

        // handle's night vision shader param
        public void OnRenderFrame(float deltaTime, EnumRenderStage stage)
        {
            float ponyNightVision = NightVisionEnabled ? NightVisionStrength : 0;
            float currNightVision = capi.Render.ShaderUniforms.NightVisionStrength;
            capi.Render.ShaderUniforms.NightVisionStrength = Math.Max(ponyNightVision, currNightVision);
        }

        public void CleanupSkinGlow()
        {
            // TODO: jannie command to fix any improper skin part glow
            // skin part glow could be messed up if players race change
            // while parts are still glowing.
        }

        /// <summary>
        /// Hotkey handler for kirin fire.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public bool OnHotkeyKirinFire(KeyCombination keys)
        {
            var player = capi.World.Player;

            // client-side check if can use
            bool canUse = kemono.HasAnyRaceTrait(player.Entity, KirinFireAllowedTraits);
            if (!canUse)
            {
                Debug.WriteLine("[Client] CANNOT USE KIRIN FIRE");
                return true;
            }

            var staminaBehavior = player.Entity.GetBehavior<EntityBehaviorPonyStamina>();
            if (staminaBehavior != null)
            {
                if (staminaBehavior.CurrentStamina < 1) return true;

                // set current client side stamina to 0 to prevent spam
                staminaBehavior.CurrentStamina = 0;
            }

            // send packet to server requesting to buck
            clientToServerChannel.SendPacket(new PacketClientRequestKirinFire());

            return true;
        }

        /// <summary>
        /// Server packet handler for player kirin fire request.
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveRequestKirinFire(IServerPlayer fromPlayer, PacketClientRequestKirinFire p)
        {
            var player = fromPlayer.Entity;

            // server-side check if can use ability
            bool canUse = kemono.HasAnyRaceTrait(player, KirinFireAllowedTraits);
            if (!canUse)
            {
                Debug.WriteLine($"[Server] {fromPlayer} CANNOT USE KIRIN FIRE");
                return;
            }

            TryKirinFire(player);
        }

        /// <summary>
        /// Hotkey handler for unicorn levitation ability.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public bool OnHotkeyLevitate(KeyCombination keys)
        {
            var player = capi.World.Player;

            // client-side check if can use
            bool canUse = kemono.HasAnyRaceTrait(player.Entity, LevitateAllowedTraits);
            if (!canUse)
            {
                Debug.WriteLine("[Client] CANNOT USE LEVITATE");
                return true;
            }

            var staminaBehavior = player.Entity.GetBehavior<EntityBehaviorPonyStamina>();
            if (staminaBehavior != null)
            {
                if (staminaBehavior.CurrentStamina < 1) return true;

                // reduce stamina on client side stamina to prevent spam
                staminaBehavior.CurrentStamina -= 0.1f;
            }

            // get targetted entity
            var target = capi.World.Player.CurrentEntitySelection?.Entity;

            if (target == null) return true;

            // send packet to server requesting to levitate
            clientToServerChannel.SendPacket(new PacketClientRequestLevitate {
                Target = target.EntityId
            });

            return true;
        }

        /// <summary>
        /// Server packet handler for player kirin fire request.
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveRequestLevitate(IServerPlayer fromPlayer, PacketClientRequestLevitate p)
        {
            var player = fromPlayer.Entity;

            // server-side check if can use ability
            bool canUse = kemono.HasAnyRaceTrait(player, LevitateAllowedTraits);
            if (!canUse)
            {
                Debug.WriteLine($"[Server] {fromPlayer} CANNOT USE LEVITATE");
                return;
            }

            // get targetted entity
            var target = sapi.World.GetEntityById(p.Target);
            if (target == null) return;

            player.GetBehavior<EntityBehaviorPonyLevitate>()?.StartLevitating(target);

            // send packet to client indicating levitate success
            serverToClientChannel.BroadcastPacket(new PacketServerLevitateStart {
                EntityId = player.EntityId,
                TargetId = target.EntityId
            });
        }

        /// <summary>
        /// Client response handler for server packet indicating levitate 
        /// successfully started on a target.
        /// </summary>
        /// <param name="p"></param>
        public void OnClientReceiveLevitateStart(PacketServerLevitateStart p)
        {
            // entity using levitate
            var entity = capi.World.GetEntityById(p.EntityId);
            if (entity == null) return;

            // get targetted entity
            var target = capi.World.GetEntityById(p.TargetId);
            if (target == null) return;

            entity.GetBehavior<EntityBehaviorPonyLevitate>()?.StartLevitating(target);
        }

        /// <summary>
        /// Send message to server that client has stopped levitating target.
        /// </summary>
        public void ClientStopLevitate()
        {
            clientToServerChannel.SendPacket(new PacketClientStopLevitate());
        }

        /// <summary>
        /// Server packet handler when client player stops levitating target.
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveStopLevitate(IServerPlayer fromPlayer, PacketClientStopLevitate p)
        {
            var player = fromPlayer.Entity;
            player.GetBehavior<EntityBehaviorPonyLevitate>()?.StopLevitating();

            // broadcast packet to other clients indicating levitate stop
            serverToClientChannel.BroadcastPacket(new PacketServerLevitateStop {
                EntityId = player.EntityId
            }, fromPlayer);
        }

        /// <summary>
        /// Client packet handler when server indicates entity has stopped
        /// levitating a target.
        /// </summary>
        /// <param name="p"></param>
        public void OnClientReceiveStopLevitate(PacketServerLevitateStop p)
        {
            var entity = capi.World.GetEntityById(p.EntityId);
            if (entity == null) return;

            entity.GetBehavior<EntityBehaviorPonyLevitate>()?.StopLevitating();
        }

        /// Client command to load skin config from json file.
        public TextCommandResult OnCmdPonyRaces(TextCommandCallingArgs args)
        {
            var player = args.Caller.Player;
            IServerPlayer splr = null;
            if (player != null)
            {
                splr = player as IServerPlayer;
            }

            string subcmd = args.Parsers[0].GetValue() as string;
            if (subcmd == null)
            {
                splr?.SendMessage(
                    GlobalConstants.GeneralChatGroup,
                    Lang.GetL(splr.LanguageCode, "ponyraces:cmd-ponyraces-help"),
                    EnumChatType.Notification
                );
                return TextCommandResult.Success();
            }

            if (subcmd == "reload")
            {
                splr?.SendMessage(
                    GlobalConstants.GeneralChatGroup,
                    Lang.GetL(splr.LanguageCode, "ponyraces:cmd-ponyraces-reload-info"),
                    EnumChatType.Notification
                );
                
                if (ReloadModConfig())
                {
                    splr?.SendMessage(
                        GlobalConstants.GeneralChatGroup,
                        Lang.GetL(splr.LanguageCode, "ponyraces:cmd-ponyraces-reload-success"),
                        EnumChatType.Notification
                    );
                }
                else
                {
                    splr?.SendMessage(
                        GlobalConstants.GeneralChatGroup,
                        Lang.GetL(splr.LanguageCode, "ponyraces:cmd-ponyraces-reload-failed"),
                        EnumChatType.Notification
                    );
                }
            }
            else
            {
                splr?.SendMessage(
                    GlobalConstants.GeneralChatGroup,
                    Lang.GetL(splr.LanguageCode, "ponyraces:cmd-ponyraces-invalid-subcmd"),
                    EnumChatType.Notification
                );
            }

            return TextCommandResult.Success();
        }
    }
}
