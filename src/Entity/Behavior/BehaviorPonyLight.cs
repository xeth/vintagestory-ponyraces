using System;
using kemono;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

namespace ponyraces
{
    public class EntityBehaviorPonyLight : EntityBehavior
    {
        // enabled flag
        public bool Enabled = false;

        // hsv color of light
        public byte[] LightHsv = new byte[] { 5, 2, 18 };

        // list of traits that allow using light
        public static string[] AllowedTraits = {"lightunicorn", "lightkirin"};
        
        public EntityBehaviorPonyLight(Entity entity) : base(entity)
        {

        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            base.Initialize(properties, typeAttributes);

            // cleanup old attribute
            var oldAttribute = entity.WatchedAttributes.GetAttribute("ponylight");
            if (oldAttribute != null && oldAttribute.GetType() != typeof(bool))
            {
                entity.WatchedAttributes.RemoveAttribute("ponylight");
            }

            Enabled = entity.WatchedAttributes.GetBool("ponylight", false);

            if (entity.World.Side == EnumAppSide.Client)
            {
                entity.WatchedAttributes.RegisterModifiedListener("stats", OnStatsChanged);
                entity.WatchedAttributes.RegisterModifiedListener("ponylight", OnClientToggle);
            }

            OnStatsChanged(); // run initial update from stats
        }

        /// <summary>
        /// Update light color HSV from stats.
        /// </summary>
        public void OnStatsChanged()
        {
            byte brightness = (byte) entity.Stats.GetBlended("ponylightBrightness");
            LightHsv[2] = brightness;
        }

        /// <summary>
        /// Update light enabled state from tree attribute.
        /// </summary>
        public void OnClientToggle()
        {
            Enabled = entity.WatchedAttributes.GetBool("ponylight", false);
        }

        /// <summary>
        /// Handles toggle on server side.
        /// </summary>
        public void Toggle()
        {
            SetEnabled(!Enabled);
        }

        /// <summary>
        /// Sets enabled state on server side.
        /// </summary>
        /// <param name="enabled"></param>
        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;
            entity.WatchedAttributes.SetBool("ponylight", Enabled);

            if (entity.World.Side == EnumAppSide.Server)
            {
                var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                if (skin != null)
                {
                    skin.SetSkinPartGlow("horn", Enabled ? 255 : 0, false);
                    entity.WatchedAttributes.MarkPathDirty("skinConfig");
                }
            }
        }

        public override void OnGameTick(float deltaTime)
        {
            if (entity.State == EnumEntityState.Inactive)
            {
                return;
            }

            base.OnGameTick(deltaTime);

            if (Enabled)
            {
                // TODO: server hunger tick every 1s
                if (entity.World.Side == EnumAppSide.Server)
                {

                }

                // if this is client, summon particles on horn attach point
                if (entity.World.Side == EnumAppSide.Client)
                {
                    // Random rand = new Random();
                    // Vec3d pos = entity.Pos.XYZ.Add(0, entity.LocalEyePos.Y, 0);
                    // Vec3d realPos = pos.AddCopy(-0.1 + rand.NextDouble() * 0.2, 0, -0.1 + rand.NextDouble() * 0.2);
                    // Vec3f velocity = new Vec3f(-0.2F + (float)rand.NextDouble() * 0.4F, 0.4F + (float)rand.NextDouble() * 2F, -0.2F + (float)rand.NextDouble() * 0.4F);
                    // entity.World.SpawnParticles(
                    //     1,
                    //     ColorUtil.ToRgba(125, 255, 255, 255),
                    //     realPos,
                    //     realPos,
                    //     velocity,
                    //     velocity,
                    //     (float)rand.NextDouble() * 0.5f + 0.5f,
                    //     0.01F,
                    //     0.5f,
                    //     EnumParticleModel.Quad
                    // );
                }
            }
        }

        public override string PropertyName()
        {
            return "ponylight";
        }
    }
}
