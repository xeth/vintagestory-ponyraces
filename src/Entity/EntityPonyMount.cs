using System;
using System.IO;
using System.Linq;
using ponyraces;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace ponyraces
{
    // 1.20 new mounting system is even more retarded

    // https://github.com/anegostudios/vssurvivalmod/tree/d56de3f2c7b627a7f7e082975611bf33856722ad/Systems/Boats
    // https://github.com/anegostudios/vssurvivalmod/blob/d56de3f2c7b627a7f7e082975611bf33856722ad/Systems/Boats/BoatSeat.cs

    // example mountable without seats, only one position:
    // https://github.com/anegostudios/vssurvivalmod/blob/a8acc04a8011006d338ac3743d26aa7d569289f7/BlockEntity/BEBed.cs
    
    public class EntityPonyMount : Entity, IMountableSeat, IMountable, IRenderer
    {

        ICoreClientAPI capi;

        // for IRenderer
        public double RenderOrder => 1.1; // makes this occur after normal entity renderers
        public int RenderRange => 9999;

        public override bool ApplyGravity
        {
            get { return false; }
        }

        public override bool IsInteractable
        {
            get { return false; }
        }

        public long PassengerEntityIdForInit { get; set; }

        // IMountableSeat can mount/unmount: make both true,
        // handle mounting conditions elsewhere in Mount.cs
        public bool CanMount(EntityAgent entityAgent) => true;
        public bool CanUnmount(EntityAgent entityAgent) => true;

        // ignore
        public bool DoTeleportOnUnmount { get; set; } = false;

        // host entity this is attached to
        public Entity Host = null;
        public EntityBehaviorPonyFlight HostPonyFlight = null;
        public long HostEntityIdForInit;

        // dont skip idle animation
        public bool SkipIdleAnimation => false;

        public float FpHandPitchFollow => 1;

        // no seat id or config needed, only single seat
        public string SeatId { get => "pony"; set { } }
        public SeatConfig Config { get => null; set { } }

        // imountable shit
        public Entity Entity
        {
            get { return Host; }
            set { Host = value; }
        }

        // entity mounted on this
        public Entity Passenger { get; set; } = null;

        // this is the mountable, no other seats
        public IMountable MountSupplier => this;

        // imountable shit
        public IMountableSeat[] Seats => new IMountableSeat[] { this };

        public bool controllable;

        protected Vec3f eyePos = new Vec3f(0, 1, 0);

        public EntityControls controls = new EntityControls();

        public EntityControls Controls
        {
            get {
                return controls; 
            }
        }

        public Entity Controller => MountedBy;

        // idk wtf this is
        public Entity OnEntity => null;

        // host attach point for passenger mount
        string MountAttachPoint = "Back";
        
        // mount position (for IMountable)
        EntityPos mountPos = new EntityPos();
        public EntityPos Position
        {
            get
            {
                var pos = Host.SidedPos;
                var moffset = MountOffset;

                mountPos.SetPos(
                    pos.X + moffset.X,
                    pos.Y + moffset.Y,
                    pos.Z + moffset.Z
                );

                mountPos.SetAngles(
                    pos.Roll,
                    pos.Yaw,
                    pos.Pitch
                );

                return mountPos;
            }
        }

        public EntityPos SeatPosition => Position;
        public Matrixf RenderTransform => null;

        public static string AnimationWhenFlying = "mountponyflying";
        public static string AnimationOnGround = "mountponyground";

        public AnimationMetaData SuggestedAnimation
        {
            get
            {
                string anim = HostPonyFlight?.IsFlying == true ? AnimationWhenFlying : AnimationOnGround;

                if (Passenger?.Properties?.Client.AnimationsByMetaCode?.TryGetValue(anim, out var ameta) == true)
                {
                    return ameta;
                }

                return null;
            }
        }

        public EnumMountAngleMode AngleMode => EnumMountAngleMode.Push;
        public new Vec3f LocalEyePos => eyePos;
        public Entity MountedBy => Passenger;
        public bool CanControl => controllable;
        public override bool CanCollect(Entity byEntity) => false;

        public EntityPonyMount()
        {
            controls.OnAction = OnControls;
        }

        public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
        {            
            base.Initialize(properties, api, InChunkIndex3d);

            capi = api as ICoreClientAPI;
            if (capi != null)
            {
                capi.Event.RegisterRenderer(this, EnumRenderStage.Before, "ponymount");
            }

            // from vanilla:
            // "The mounted entity will try to mount as well, but at that time,
            // the boat might not have been loaded, so we'll try mounting on both ends.
            // (wtf?)
            if (HostEntityIdForInit != 0 && Host == null)
            {
                if (api.World.GetEntityById(HostEntityIdForInit) is EntityAgent entity)
                {
                    Host = entity;
                    HostPonyFlight = Host.GetBehavior<EntityBehaviorPonyFlight>();
                }
            }
            if (PassengerEntityIdForInit != 0 && Passenger == null)
            {
                if (api.World.GetEntityById(PassengerEntityIdForInit) is EntityAgent entity)
                {
                    entity.TryMount(this);
                }
            }

            // set host mount entity
            if (HostPonyFlight != null)
            {
                HostPonyFlight.MountEntity = this;
            }
        }

        public override void OnGameTick(float dt)
        {
            EntityPlayer hostPlayer = Host as EntityPlayer;
            bool hostIsOnline = hostPlayer?.Player != null;
            bool hostIsFlying = HostPonyFlight?.IsFlying == true;

            // conditions to remove this mount entity when no longer needed
            if (
                Host == null ||
                Host?.State != EnumEntityState.Active ||
                Host?.Alive == false ||
                hostIsOnline == false ||
                (hostIsFlying == false && Passenger == null)
            ) {
                (Passenger as EntityAgent)?.TryUnmount();
                Die(EnumDespawnReason.Removed);
                return;
            }

            // Sync position to host entity
            if (Host != null)
            {
                SidedPos.SetFrom(Host.SidedPos);
            }

            // play animation
            // TODO: can we make this more efficient?
            if (Api.Side == EnumAppSide.Client)
            {
                var animManager = Passenger?.AnimManager;
                if (animManager != null)
                {
                    string currentAnimation;
                    string otherAnimation;
                    if (hostIsFlying)
                    {
                        currentAnimation = AnimationWhenFlying;
                        otherAnimation = AnimationOnGround;
                    }
                    else
                    {
                        currentAnimation = AnimationOnGround;
                        otherAnimation = AnimationWhenFlying;
                    }

                    animManager.StopAnimation(otherAnimation);
                    if (!animManager.IsAnimationActive(currentAnimation))
                    {
                        animManager.StartAnimation(new AnimationMetaData()
                        {
                            Code = currentAnimation,
                            Animation = currentAnimation,
                            AnimationSpeed = 1f,
                            EaseInSpeed = 0.2f,
                            EaseOutSpeed = 0.2f,
                            Weight = 1f
                        });
                    }
                }
            }
            
            base.OnGameTick(dt);
        }

        public virtual void OnRenderFrame(float dt, EnumRenderStage stage)
        {
            // Client side we update every frame for smoother turning
            if (capi.IsGamePaused) return;

            // Sync position to host entity
            if (Host != null && Passenger != null)
            {
                if (Passenger == capi.World.Player.Entity)
                {
                    // own player sync:
                    // set the retarded extra camerapos THANKS SNEEZER :DDDD
                    var mountPos = Position; // mount position
                    capi.World.Player.Entity.CameraPos.Set(mountPos);
                }
                else
                {
                    // other entity sync:
                    // dont do on passenger player's own client because this makes it
                    // impossible for player to rotate camera and do stuff

                    // force player camera pos for rendering to real pos otherwise
                    // player entity stutters THANKS SNEEZER :DDDD
                    if (Host == capi.World.Player.Entity)
                    {
                        capi.World.Player.Entity.CameraPos.Set(Host.SidedPos);
                    }

                    SidedPos.SetFrom(Host.SidedPos);

                    // Console.WriteLine($"Mount: {EntityId} {Host.EntityId} {Passenger.EntityId} mount? = {Passenger.MountedOn != null}");
                    var mountPos = Position; // mount position
                    Passenger.Pos.SetFrom(mountPos);
                    // Console.WriteLine($"[Mount.OnRenderFrame] passenger pos: {Passenger.Pos}");

                    var passengerAsEntityAgent = Passenger as EntityAgent;
                    if (passengerAsEntityAgent != null)
                    {
                        var hostAsEntityAgent = Host as EntityAgent;
                        if (hostAsEntityAgent != null)
                        {
                            // have to use body yaw because host is doing
                            // entity interpolation:
                            // https://github.com/anegostudios/vsessentialsmod/blob/0ab11798b25e6d9c5ab45f1ec42945fe5568f4d3/Entity/Behavior/BehaviorInterpolatePosition.cs#L280
                            passengerAsEntityAgent.BodyYaw = hostAsEntityAgent.BodyYaw;
                        }
                        else
                        {
                            passengerAsEntityAgent.BodyYaw = Host.SidedPos.Yaw;
                        }
                    }
                }
            }
        }

        public override void OnInteract(EntityAgent byEntity, ItemSlot itemslot, Vec3d hitPosition, EnumInteractMode mode)
        {
            return;
        }

        /// <summary>
        /// TODO: this crashes when player sneak clicks player wtf tyron
        /// </summary>
        /// <param name="action"></param>
        /// <param name="on"></param>
        /// <param name="handled"></param>
        public void OnControls(EnumEntityAction action, bool on, ref EnumHandling handled)
        {
            // unmount on client, send to server
            // because server does not recognize this action lmao
            // also Api might not be loaded or some shit when this is called
            // if the player sneak clicks idk fucking retarded
            if (Api?.Side == EnumAppSide.Client)
            {
                if (Passenger != null && action == EnumEntityAction.Sneak && on)
                {
                    (Passenger as EntityAgent)?.TryUnmount();
                    controls?.StopAllMovement();
                    Api.ModLoader.GetModSystem<PonyRaceMod>()?.ClientUnmountRequest(EntityId);
                }
            }
        }

        public override void ToBytes(BinaryWriter writer, bool forClient)
        {
            base.ToBytes(writer, forClient);
            writer.Write(Host?.EntityId ?? 0);
            writer.Write(Passenger?.EntityId ?? 0);
        }

        public override void FromBytes(BinaryReader reader, bool fromServer)
        {
            base.FromBytes(reader, fromServer);

            long hostId = reader.ReadInt64();
            HostEntityIdForInit = hostId;

            long entityId = reader.ReadInt64();
            PassengerEntityIdForInit = entityId;
        }

        public virtual bool IsEmpty()
        {
            return Passenger == null;
        }

        public override WorldInteraction[] GetInteractionHelp(IClientWorldAccessor world, EntitySelection es, IClientPlayer player)
        {
            return base.GetInteractionHelp(world, es, player);
        }

        readonly Vec3f ZeroMountOffset = new Vec3f(0, 0, 0);

        public Vec3f MountOffset
        {
            get
            {
                if (Host == null) return ZeroMountOffset;

                AttachmentPointAndPose apap = Host.AnimManager.Animator?.GetAttachmentPointPose(MountAttachPoint);
                // Console.WriteLine($"MountOffset: {EntityId} {Host.EntityId} {apap} {apap?.AttachPoint}");
                if (apap != null && apap.AttachPoint != null)
                {
                    var ap = apap.AttachPoint;
                    float rotX = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateX : 0;
                    float rotY = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateY : 0;
                    float rotZ = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateZ : 0;
                    float bodyYaw = (Host is EntityAgent) ? (Host as EntityAgent).BodyYaw : 0;
                    float bodyPitch = (Host is EntityPlayer) ? (Host as EntityPlayer).WalkPitch : 0;
                    
                    // Console.WriteLine($"[{entity}] rotX: {rotX}, rotY: {rotY}, rotZ: {rotZ}, bodyYaw: {bodyYaw}, bodyPitch: {bodyPitch} entity.SidedPos.Yaw: {entity.SidedPos.Yaw} entity.SidedPos.Roll: {entity.SidedPos.Roll}");
                    // bodyYaw = (entity is EntityPlayer) ? (bodyYaw - (float)Math.PI/2) : bodyYaw; // FUCK THIS ENGINE SO FUCKING MUCH

                    // final yaw offset
                    float yawOffset = (Host is EntityPlayer) ? bodyYaw : Host.SidedPos.Yaw;
                    yawOffset += (float) Math.PI/2; // retarded

                    var mat = new Matrixf()
                        .RotateX(Host.SidedPos.Roll + rotX * GameMath.DEG2RAD)
                        .RotateY(yawOffset + rotY * GameMath.DEG2RAD)
                        .RotateZ(bodyPitch + rotZ * GameMath.DEG2RAD)
                        .Scale(Host.Properties.Client.Size, Host.Properties.Client.Size, Host.Properties.Client.Size)
                        .Translate(-0.5f, 0, -0.5f) // piece of shit engine
                        .Translate(ap.PosX / 16f, ap.PosY / 16f, ap.PosZ / 16f) // not needed anymore? 1.19
                        .Mul(apap.AnimModelMatrix)
                    ;

                    float[] pos = new float[4] { 0, 0, 0, 1 };
                    float[] endVec = Mat4f.MulWithVec4(mat.Values, pos);

                    float dx = endVec[0];
                    float dy = endVec[1];
                    float dz = endVec[2];

                    // Console.WriteLine($"apap: {ap.Code} {apap}, {apap.AnimModelMatrix} {mat}");
                    // Console.WriteLine($"ap: {ap.Code}, x,y,z = {ap.PosX}, {ap.PosY}, {ap.PosZ}");
                    // Console.WriteLine($"ap: {ap.Code} dx,dy,dz = {dx}, {dy}, {dz}");

                    return new Vec3f(dx, dy, dz);
                }
                else
                {
                    return ZeroMountOffset;
                }
            }
        }

        public void DidMount(EntityAgent entityAgent)
        {
            // Console.WriteLine($"DidMount, Passenger={Passenger}, entityAgent={entityAgent}");

            if (Passenger != null && Passenger != entityAgent)
            {
                (Passenger as EntityAgent)?.TryUnmount();
                return;
            }

            Passenger = entityAgent;
        }
        
        public void DidUnmount(EntityAgent entityAgent)
        {
            // Console.WriteLine($"DidUnmount, Passenger={Passenger}, entityAgent={entityAgent}");

            // // write stack trace leading to this
            // var stackTrace = new System.Diagnostics.StackTrace();
            // Console.WriteLine(stackTrace.ToString());

            if (Passenger == null) return;

            RemovePassenger();

            // you should kill yourself NOW
            Die(EnumDespawnReason.Removed);
        }

        public override void OnEntityDespawn(EntityDespawnData despawn)
        {
            base.OnEntityDespawn(despawn);

            // Api.Logger.Notification($"DESPAWNING {EntityId} {despawn.Reason}");

            // remove host flight link
            if (HostPonyFlight != null)
            {
                HostPonyFlight.MountEntity = null;
            }

            // cleanup passenger
            RemovePassenger();
        }

        public void RemovePassenger()
        {
            EntityAgent passenger = Passenger as EntityAgent;
            if (passenger == null) return;
            Passenger = null;

            // note this re-calls DidUnmount, need Passenger == null
            // check to avoid this recursing infinitely
            passenger.TryUnmount();

            passenger.Pos.Roll = 0;

            passenger.AnimManager?.StopAnimation(AnimationWhenFlying);
            passenger.AnimManager?.StopAnimation(AnimationOnGround);
        }

        public void MountableToTreeAttributes(TreeAttribute tree)
        {
            tree.SetString("className", "ponymount");
            tree.SetLong("entityId", EntityId);
        }
        
        public static IMountableSeat GetMountable(IWorldAccessor world, TreeAttribute tree)
        {
            return world.GetEntityById(tree.GetLong("entityId")) as EntityPonyMount;
        }

        public void Dispose()
        {
            
        }

        public bool AnyMounted() => MountedBy != null;
    }
}
